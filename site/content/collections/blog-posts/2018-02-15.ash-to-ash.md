author: cbd742bc-c33e-4489-b4dd-c43f476c607c
feature_image: /assets/img/blog-posts/ahna-ziegler-558901-unsplash.jpg
excerpt: >
  Last night the kids and I went to our very first Ash Wednesday service. It was, with no hyperbole,
  one of the five or six most impactful services of my life. As someone who has grown up in a stream
  of Christianity that does not engage in liturgy or observe the church calendar (other than Christmas
  and Easter obviously) I thought offering some of my thoughts from the experience would be helpful
  and enlightening to some.
post_content:
  - 
    type: text_block
    text: |
      <p>Ash Wednesday, for those who aren’t familiar with it, is the first day of Lent and kicks of a season of repentance and anticipation leading up to Good Friday and Easter. It is meant to turn our eyes towards the soon approaching cross of Christ where our sins were atoned for and mercy was released instead of judgement. Traditionally the service is highlighted by receiving ashes in the shape of the cross on your hand or forehead and then corporately repenting of sin. This was all new to me.
      </p>
      <p>Here is why I found it so moving and transformative:
      </p>
      <p>It is one of the only times, maybe the only time because I honestly can’t think of any others right now, that I felt and experienced the full gospel lived out and embodied. During that 60 minute service the gospel was incarnate, it took on flesh, my flesh.
      </p>
      <p>Beginning with songs of worship I felt connected to the God who formed me and made me in His image. This was followed by a short homily (message) on our need for repentance and to be made new. Then we went forward to receive the ashes of repentance. As the elder made the symbol of the cross on my forehead and called me to repent of my sins I felt, in my own flesh, blood, and bone, my utter need for salvation. I confessed my sins of failing to love God and neighbor, of serving the idols of my flesh and this world, and having a wayward heart that to often goes after other lovers. As I knelt on the cold concrete with Kelles in my arms and Laikyn kneeling next to me I was living out my own need for a savior.
      </p>
      <p>Then the good news!
      </p>
  - 
    type: pullquote
    quote: “During that 60 minute service the gospel was incarnate, it took on flesh, my flesh.”
    cite: — MAXWELL G. THOMAS
  - 
    type: text_block
    text: |
      <p>At the announcement of the pastor we all rose from our knees, which symbolized the grave of our sin where we had been buried with Christ, and stood to our feet just as we have been raised from the dead by the resurrection of Christ. Now standing together, as the resurrected people of God, with the sign of the cross on our foreheads marked by the ashes of our frailty and of our death in Christ, we raised our hands and received forgiveness. That's when it him me, we are new creations! We were made from ashes and in the death of Christ we returned to those ashes. But now we stand as a resurrected people and as God’s new creation in the world.
      </p>
      <p>We ended the night singing praise to Christ who has shown us mercy. The man in front of me was weeping tears of joy as he received the grace of God after grappling with his own sin. The couple behind me sang their hearts out worshipping the Christ who redeemed them.
      </p>
      <p>I’ve heard the gospel. I’ve spoken the gospel. But last night, through the ancient practices and liturgies of the historic church, I lived the gospel in my own body. In these practices of receiving the ash, repeating the ancient prayers of corporate confession and repentance, and rising to my feet in the Spirit of Resurrection to newness of life, I encountered the gospel through the Spirit-filled movements of my body and the confession of my own lips.
      </p>
      <p>From ash you were made and to ash we have returned in and through Christ. But now we have be raised with him as His new creation still marked with the ashes of our death in Him as a witness to a dying world that sin and death have been conquered in Christ.
      </p>
title: Ash To Ash
id: 47435472-44f0-4b3a-a47f-7ec06c79ac74
