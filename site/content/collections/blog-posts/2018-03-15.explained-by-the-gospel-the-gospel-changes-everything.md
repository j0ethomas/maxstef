author: cbd742bc-c33e-4489-b4dd-c43f476c607c
feature_image: /assets/img/blog-posts/ross-findon-303091-unsplash.jpg
excerpt: 'This is part one of a small series of blogs I’ll be doing entitled: Explained by the Gospel.'
post_content:
  - 
    type: text_block
    text: |
      <p><em>This is part one of a small series of blogs I’ll be doing entitled: Explained by the Gospel.</em>
      </p>
      <p>My small group has recently been wrestling with how to better be a gospel witness to our own city. As part of a missionary organization we are constantly thinking about how to do this overseas but we don’t want to become detached from our own immediate context. As a group we’ve moved well beyond strategy and task into core conviction and belief. Our conviction will not only inform our strategy but fuel it.
      </p>
      <p>Here is my core conviction: <em>The gospel changes everything.</em>
      </p>
      <p>There are three parts to that statement and all three frighten me.
      </p>
      <h2>The gospel </h2>
      <p><em><strong></strong></em>By this I mean that the crucified Jesus is the risen Lord and rightful King of all the earth. That announcement, if true, hangs over everything. It is a surging torrent that floods everything in its path, including me. I am beholden to it. I am subject to it. I am accountable to it. And so are you.
      </p>
      <h2>Changes</h2>
      <p><em><strong></strong></em>I, like you, don’t care for change especially when I am not the one dictating the change. The gospel announcement demands change. It requires repentance, a rethinking in its light.
      </p>
      <h2>Everything</h2>
      <p><em><strong></strong></em>This gospel lays hold of absolutely everything. There is nothing outside of its scope, nothing out of its reach, nothing that Christ does not claim as His. The slain and risen Lamb beckons us, in light of His loving victory, to lay our entire being before Him. If He is Lord and King than I no longer hold anything of my own.
      </p>
      <p>This statement is especially difficult for us who have grown up under the tutelage of “unalienable rights” to life, liberty, and the pursuit of happiness. Our entire civilization and culture are built upon these rights and our God-given empowerment to pursue and hold on to them. The core of American identity can be summed up in the single word of <em>rights</em>.
      </p>
      <p>On both sides of the isle we wave our rights high above everything else. Don’t tell me what to say because I have the right to free speech. Don’t talk about gun control because I have the right to bear arms. Don’t raise my taxes because I have the right to my money. Don’t tell me what to do with my body because I have the right to use it however I want to. And the list goes on and on.
      </p>
      <p>The idea that the gospel changes everything flies in the face of American rights. But if the gospel is true than it is foolish, and arrogant, to stand at the foot of the cross demanding your unalienable rights. If you can look at the crucified Christ while insisting our your personal rights to life, liberty, and the pursuit of happiness it is probably because you have pledged allegiance to something other than Jesus and His kingdom.
      </p>
      <p>So how does this statement inform, shape, and fuel the way we live in our immediate context? I’ll answer that question with another question and then explain:
      </p>
      <p>Answer:  <em>Does your life demand a gospel explanation?</em>
      </p>
      <p>If the gospel does indeed change everything than it should be the gospel that explains everything. The gospel should be the answer to all of the why questions concerning your life and lifestyle.
      </p>
      <p>Why spend your money this way? <em>The gospel.</em>
      </p>
      <p>Why go here? <em>The gospel.</em>
      </p>
      <p>Why do you help those people? <em>The gospel.</em>
      </p>
      <p>Why don’t you do this with the rest of us? <em>The gospel.</em>
      </p>
      <p>Why are you so forgiving? Merciful? Loving? Open? Thoughtful? Gracious? Accommodating? Hospitable? Generous? <em>THE GOSPEL.</em>
      </p>
      <p>And so I ask again, does your life demand a gospel explanation? Are you willing relinquish your rights for the sake of the gospel? And are you willing to rearrange your life for the spread of the gospel?
      </p>
      <p>Let's start rethinking everything together.
      </p>
  - 
    type: pullquote
    quote: If the gospel does indeed change everything than it should be the gospel that explains everything.
    cite: '- Maxwell G. Thomas'
title: 'Explained by the Gospel: The Gospel Changes Everything'
id: 6d7a3c6b-e180-4d11-aba4-4211238c2543
