author: cbd742bc-c33e-4489-b4dd-c43f476c607c
feature_image: /assets/img/blog-posts/sunset-1511933841.jpg
excerpt: >
  Destiny, it seems to the the holy grail of Christian ideals; the golden gun that would slay every
  foe with a single bullet. To some it seems like pure myth while to others it is a reality that has
  simply alluded them to this point. Regardless of what side you land on many are left feeling empty
  and purposeless. What’s my purpose? What is God’s will for my life? What is my destiny? These
  are the questions that haunt many believers like a nightmare, especially young ones as they are just
  starting their journey.
post_content:
  - 
    type: text_block
    text: |
      <p>Destiny, it seems to the the holy grail of Christian ideals; the golden gun that would slay every foe with a single bullet. To some it seems like pure myth while to others it is a reality that has simply alluded them to this point. Regardless of what side you land on many are left feeling empty and purposeless.  What’s my purpose? What is God’s will for my life? What is my destiny? These are the questions that haunt many believers like a nightmare, especially young ones as they are just starting their journey.
      </p>
      <p>The question of personal destiny and purpose is an interesting one because, at least through my observation, it is both a universal question that people across time and place are asking while at the same time being a heightened point of emphasis within certain portions of Christianity, particularly charismatic evangelicalism (the group I belong to). This second phenomenon is far more interesting to me.
      </p>
      <p>It seems like every other message from mainstream charismatic pastors revolves around finding and fulfilling your individual destiny or purpose. The basic sermon structure goes something like this:
      </p>
      <ol>
      <li>A short text from Scripture.</li>
      <li>Pull a principle from that text.</li>
      <li>Spend the majority of the message on how that principle will empower you to fulfill your destiny.</li>
      </ol>
      <p>This message of finding and fulfilling your personal God ordained destiny has birthed the now common tool for evangelism which is to tell a sinner that “God loves you and has a destiny for your life”. But most do that  without ever defining any of those terms or ideas. Consequently those who once chased the American dream of wealth and success now chase the americanized christian ideal of destiny and purpose. Sadly these look far too similar far too often. It’s akin to a self-serve yogurt shop simply switching out the flavors on its customers. You once ate the world’s strawberry and vanilla yogurt and now you eat the chocolate and berry flavors the church has to offer. But the issue is that you are still at a self-serving yogurt shop. You are still serving yourself.
      </p>
      <p>When I talk to young people about their life and future this idea of purpose usually comes up in relation to their desired career, family or location. And while I understand their sincere desire to be in God’s will (and I would never want to stifle that desire) to associate any of those things with your ultimate destiny is biblically incorrect. You’ve simply started eating different flavor of self-serve yogurt. If my ultimate destiny and purpose is tied to a job, location, ministry or family status then, to be quite frank, I’ve missed understood God’s will completely.
      </p>
      <p>The logic is that before I was saved my ultimate joy and fulfillment was found in money, titles, security and comfort but that wasn’t working. Now I’m saved, and I’ve been told that God must have a better job, more security, and complete comfort for me because He loves me and cares for me. Thus my God simply becomes an additive, a steroid booster of sorts, by which I can truly attain my dreams and destiny.
      </p>
      <p>Sadly this golden gun doesn’t exist. That nagging gnawing feeling inside will never go away with any type of promotion, any amount of money, complete security and comfort, or any other sort of earthly thing. We’ve been told that everyone has a unique destiny and purpose and to some extent that is true because every individual life is different. However when you turn that individual destiny into your ultimate destiny you will never be satisfied and you will always second guess every decision.
      </p>
      <p>The truth is your ultimate destiny is not unique it’s the same as mine and everyone else’s. We were all created to exalt God and enjoy Him forever. That is your destiny. That is your purpose. And you can do that in any job, in any country, with complete peace and security or chaos and danger all around. You can fulfill that destiny whether you are rich or poor, have all things or nothing at all, have the most exciting career or a mundane nine to five.
      </p>
      <p>The golden gun, the master key, and hidden treasure you’ve been searching for is not a better this or more of that it is, completely and solely, to live a life that wholly exalts God and is completely satisfied in Christ. No more and no less.  So when thinking about your life, your future, your job, your money, your family, your anything ask yourself does this lead me or help me glorify and enjoy God?  And if that job, or that money, or that thing added to your schedule is going to diminish your ability to fully exalt and enjoy God then I can unequivocally say it is not God’s will or your destiny.
      </p>
title: 'Destiny: A Man With A Golden Gun'
id: 5fcb6771-270c-4b22-bcdf-49b9cf3d6803
