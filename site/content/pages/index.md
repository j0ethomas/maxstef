hero_headline: >
  “And in this way I desire to preach where Christ has not been named, so as not to build on another
  person’s foundation.”
cite: The Apostle Paul
intro_headline: Living a Life That Requires a Gospel Explanation
intro_content: >
  What is Christ worth? To us, He is worth everything. It is this supreme value that drives us forward
  and is propelling us to give our lives to see the Gospel go forth into the nations. Our heart is
  specifically stirred for those people groups around the world who are unreached and have little to
  no access to the Gospel. Our desire is both to go to them and preach the good news and to help stir
  a new missions movement that covers the globe and makes famous the name of Jesus Christ.
video: 'https://www.youtube.com/embed/UwhlKeyilfc?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay'
stat_photo: /assets/img/aditya-siva-155282@2x.jpg
block_1_photo: /assets/img/home-page-blocks/man-in-window@1x.jpg
block_1_headline: "They don't believe because they've never heard."
block_1_content: '3.16 billion people (6943 people groups) are still considered unreached with the gospel; they have had some missionary work however they still have little to no  active native churches.'
block_2_photo: /assets/img/home-page-blocks/aditya-siva-155282@1x.jpg
block_2_headline: "They've never heard because no one has ever told them."
block_2_content: "1510 people groups are still considered unengaged with the gospel, meaning they have never had a missionary work in their people's history. Additionally these people have no access to reach the gospel, a church, or a Christian even if they wanted to. They are completely cut off from God. "
block_3_photo: /assets/img/home-page-blocks/1498275256729@1x.jpg
block_3_headline: The need is great and the task is clear.
block_3_content: 'Globally there are 400,000 Christian missionaries. However only 3% of those missionaries (roughly 12,000) serve the 3.16 billion people that are still considered unreached. Tragically those who need the gospel the most have no one coming to tell them about Jesus. '
home_video: https://youtu.be/UwhlKeyilfc
text_2_headline: "They've never heard because no one has ever told them."
text_2_content: "1510 people groups are still considered unengaged with the gospel, meaning they have never had a missionary work in their people's history. Additionally these people have no access to reach the gospel, a church, or a Christian even if they wanted to. They are completely cut off from God. "
photo_blocks:
  - 
    type: photo_block_right
    photo: /assets/img/man-in-window@1x.jpg
title: Home
layout: home
template: home
fieldset: home
id: 56a08fa3-7a7f-4187-8994-38ba6b16c672
