intro: >
  Our sole aim is to display the supreme value of Christ among those who have never heard the gospel
  before so that songs of worship would arise to Jesus from every tribe, tongue, nation, and people.
family_photo: /assets/img/thomas+newborn-0043.jpg
max_photo: /assets/img/max-thomas.jpg
max_bio: >
  With grandparents who were pioneer missionaries in Papua New Guinea, missions run deep in my blood.
  It is my passion to raise up and send out missionaries who have deep theological roots in the
  Scriptures and a burning life of prayer and fasting for the lost.
stef_photo: /assets/img/stef-thomas.jpg
stef_bio: >
  After attending Iris School of Missions in Mozambique in 2011 my heart was set a blaze for missions.
  Ever since then it has been my dream to use my education as a R.N. and a Certified Nurse Midwife to
  both preach the gospel and bring aid to those most in need.
laik_photo: /assets/img/laik-thomas.jpg
laik_bio: >
  Born August 5, 2015, Laikyn is named after the famous missionary and healing evangelist John G.
  Lake. Even at a young age she loves to pray, worship, and read books.
kelles_photo: /assets/img/kelles-thomas.jpg
kelles_bio: >
  Born May 25, 2017, Kelles is named after his great grandpa who was a pioneer missionary to PNG. He
  is the first grandson on either side of the family so needless to say, he is pretty popular.
title: Our Family
fieldset: family
layout: page
template: family
id: 2592720b-5059-41de-aec9-449c53fa2117
