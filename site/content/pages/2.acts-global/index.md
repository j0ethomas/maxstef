about_acts_photo: /assets/img/about-acts-1512197456.jpg
about_acts_headline: What if we were to finish The Great Commission in our generation?
about_acts_body: >
  The Antioch Center for Training and Sending (ACTS) is a missional community and movement based out
  of Colorado Springs, CO that exists to equip and send 10,000 young pioneering leaders to finish the
  task of world evangelization, make disciples, and sing back the King.
our_role_photo: /assets/img/priscilla-du-preez-201729-1512197486.jpg
our_role_headline: Leading a new Student Volunteer Movement.
our_role_body: >
  As part of the ACTS Senior Leadership Team we serve as teachers in the missions school, bring
  oversight to the global prayer room, which sustains extended hours of daily prayer for God to raise
  up missionaries and send them out in His harvest field, and provides pastoral care to the ACTS
  community.
title: Acts Global
template: acts
fieldset: acts-global
layout: page
id: f4a35112-48a4-425b-aaac-e8c836f46e63
